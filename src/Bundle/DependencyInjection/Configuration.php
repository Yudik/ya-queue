<?php

namespace Akson\Bundle\SQSQueue\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder("akson_sqs");

        $treeBuilder
            ->getRootNode()
                ->children()
                    ->scalarNode("queue_name")
                        ->isRequired()
                        ->cannotBeEmpty()
                    ->end()
                    ->scalarNode("version")
                        ->defaultValue("latest")
                    ->end()
                    ->scalarNode("region")
                        ->defaultValue("ru-central1")
                    ->end()
                    ->scalarNode("endpoint")
                        ->defaultValue("https://message-queue.api.cloud.yandex.net")
                    ->end()
                ->end();

        return $treeBuilder;
    }
}